# file-system-explorer #

`FileSystemExplorer` is an utility class with which you can:

* Create `JSON object` from the file system

* Get all `files/directories details` from the file system

* Get `list of files/directories`

* Create directory

* Delete file/directory

* Rename file/directory

* Check `free mount space`

from the given path and many more(s).

Installation
------------

    npm install file-system-explorer

Usage
-----

#### How to import file-system-explorer? ####
```js
    const fileSystemExplorer = require('file-system-explorer');
```
or
```ts
    import { FileSystemExplorer } from 'file-system-explorer';
```
After import, create an object of FileSystemExplorer class.
```js
    // For JS
    const fsExplorer = new fileSystemExplorer.FileSystemExplorer();
```
or
```ts
    // For TypeScript
    const fsExplorer: FileSystemExplorer = new FileSystemExplorer();
```

##### Create JSON object from a given path (tree like structure) #####
```js
// return JSON object for directory tree
const myPathTree = fsExplorer.createFileSystemTree('/path/to/dir/or/file');
```

##### Get All Directories path from a prepared Directory Tree #####
```js
const myPathTree = fsExplorer.createFileSystemTree('/path/to/dir/or/file');
// returns string[]
const myPathDirectories = fsExplorer.getAllDirectoryPathFromPreparedFSInfo(myPathTree);
```

##### Get All Directories path from a given path (includes directories from the sub-directories) #####
```js
// returns string[]
const myPathDirectories = fsExplorer.getAllDirectoryPathFromFileSystem('/path/to/dir/or/file'); 
```

##### Get All Files path from a prepared Directory Tree #####
```js
    const myPathTree = fsExplorer.createFileSystemTree('/path/to/dir/or/file');
    const myPathFiles = fsExplorer.getAllFilePathFromPreparedFSInfo(myPathTree); // returns string[]
```

##### Get All Files path from a given path (includes files from the sub-directories) #####
```js
    const myPathFiles = fsExplorer.getAllFilePathFromFileSystem('/path/to/dir/or/file'); // returns string[]
```

##### Get All Files and Directories details from a given path (include all files/directories from sub directories) #####
```js
// returns array of File/Directory details IFSInfo[]
// Details include size, file name, type, last modified time, extension, complete path
const myPathAllFSDetails = fsExplorer.getAllFSElementsDetailsFromFileSystem('/path/to/dir/or/file');
```

##### Get All Files and Directories details from a prepared Directory Tree #####
```js
const myPathTree = fsExplorer.createFileSystemTree('/path/to/dir/or/file');
// returns array of File/Directory details IFSInfo[]
const myPathAllFSDetails = fsExplorer.getAllFSElementsDetailsFromPreparedFSInfo(myPathTree);
```

##### Get All Files and Directories details from the given path #####
```js
// returns array of File/Directory details IFSInfo[]
const myPathContents = fsExplorer.getDirectoryContentList('/path/to/dir');
```

##### Create Directory in a Given path #####
```js
// creates 'New Folder' directory inside '/path/to/dir'
const mkdirResponse = createDirectory('/path/to/dir/New Folder', true);
// creates 'New Folder' directory inside '/path/to/dir', if already exists, then will create 'New Folder (1)'
const mkdirResponse = createDirectory('/path/to/dir');
// Return mkdirResponse.errno 0 on success
```

##### Delete Directory/Files #####
```js
// To delete directory, pass second paramater as true
const deleteDirResponse = deleteDirectory('/path/to/dir', true);
const deleteDirResponse = deleteDirectory('/path/to/file');
// Returns errno 0, on success
```

##### Rename Directory/Files #####
```js
// pass rename File/Directory details IRenameFileDetails in function parameter
// IRenameFileDetails include old_path_length, old_path_, new_path_length, new_path
const renameFSResponse = renameFile('/path/to/old/dir/or/file', '/path/to/new/dir/or/file');
// Returns errno 0, on success
```

##### Check Directory Read/Write Persmission #####
```js
const directoryRWPermRes = checkDirectoryReadWritePersmission('/path/to/dir');
// Returns errno 0, on success
```

##### Check FS Element Exists #####
```js
// check if file exists or not
const fsElementExistsResponse = checkFSElementExists('/path/to/dir/or/file');
// Returns true, on FS Element exist
```

##### Get FS Element Details #####
```js
// Returns FS Element IFSInfo 
// Details include size, file name, type, last modified time, extension, complete path
const fsElementDetails = getFSElementDetails('/path/to/dir/or/file');
```

##### Check Mount Free Space #####
```js
// Check if free space available for the mount in which this file is present
const freeSpaceInBytes = checkMountFreeSpace('/path/to/dir/or/file');
// Return free space available in bytes for mount which contain the file. If command fails, return 0
```
