import { FileSystemExplorer } from './FileSystemExplorer';

const fsExplorer: FileSystemExplorer = new FileSystemExplorer();

// console.log(fsExplorer.getAllDirectoryPathFromFileSystem('/home/abby/Downloads'));
// console.log(fsExplorer.getAllDirectoryPathFromFileSystem('/home/abby/Downloads', { exclude: /\/\.[^/]+$/g }));
// console.log(fsExplorer.getAllFilePathFromFileSystem('/home/abby/Downloads', { exclude: /\/\.[^/]+$/g }));
// console.log(fsExplorer.getAllFilePathFromFileSystem('/', { resolveSymbolicLinks: true, maxNumberOfFiles: 100000}).length);
// console.log(fsExplorer.getAllFSElementsDetailsFromFileSystem('/home/test/Desktop/some_files/test.deb', { resolveSymbolicLinks: false}));

// console.log(fsExplorer.checkMountFreeSpace('/home/abby/Downloads'));
// console.log(JSON.stringify(fsExplorer.createFileSystemTree(`/`, { resolveSymbolicLinks: true, maxNumberOfFiles: 50000 })));
// console.log(JSON.stringify(fsExplorer.createFileSystemTree(`/home/abby/Downloads`, { resolveSymbolicLinks: true })));
console.log(fsExplorer.getDirectoryContentList('/home/anmol'));
