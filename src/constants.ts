export const FS_OK: number = 0;

export const FS_ERR: number = -1;

export interface IFSOperationResponse {
    errno: number; // zero means success
    completePath: string;
    fileName: string;
}

export interface IRenameFileDetails {
    old_path_length: number;
    old_path: string;
    new_path_length: number;
    new_path: string;
}

export enum FILE_TYPE {
    FILE = 0,
    DIRECTORY = 1,
    SYMB_LINK = 2,
    DUMMY = 3,
    DRIVE = 4
}

export interface IFileSystemExplorerOptions {
    extensions?: RegExp;
    exclude?: RegExp | RegExp[];
    normalizePath?: boolean;
    resolveSymbolicLinks?: boolean;
    maxNumberOfFiles?: number;
}

export interface IFIleSystemTree {
    size: number;
    path: string;
    name: string;
    // tslint:disable-next-line: no-reserved-keywords
    type: FILE_TYPE;
    mtime: Date; // modified time
    atime: Date; // accessed time
    ctime: Date; // created time
    // [attributes: string]: any;
    childCount?: number;
    children?: IFIleSystemTree[];
    extension?: string;
}

// FS Elements means Directories or Files
export interface IFSInfo {
    size: number;
    name: string;
    // tslint:disable-next-line: no-reserved-keywords
    type: number;
    mtime: string; // modified time
    extension?: string;
    path?: string;
    childCount?: number;
}

// New format to populate files with JSON (only property names are changed as compared to IFSInfo)
export interface IFSNewInfo {
    lsize: string;
    m_name: string;
    nType: number;
    m_lmd: string; // modified time
    extension?: string;
    m_FullPath?: string;
    childCount?: number;
}
