import { execSync } from 'child_process';
import fs from 'fs';
import path from 'path';
import {
    FILE_TYPE, FS_ERR, FS_OK, IFileSystemExplorerOptions, IFIleSystemTree,
    IFSInfo, IFSNewInfo, IFSOperationResponse
} from './constants';

/**
 * Utility class to get information from File System.
 * Get Directory Tree using absolute path
 * Get All Files/Directories list from the given absolute path
 * All parameters with name path, should always be an absolute path
 */
export class FileSystemExplorer {

    constructor() {
        // members to be added in future, if any
    }

    /**
     * Collects the files and directores from a directory path into an Object
     * @param onEachFile this function can used to simultaneously send information to other party like
     * type, side, name length, file size and file
     * @param onEachDirectory this function can used to simultaneously send information to other party
     */
    public createFileSystemTree(rootPathToDir: string, options?: IFileSystemExplorerOptions, onEachFile?: Function,
                                onEachDirectory?: Function): IFIleSystemTree | undefined {

        if (options !== undefined && options.maxNumberOfFiles !== undefined) {
            if (options.maxNumberOfFiles === 0) {
                return;
            } else {
                options.maxNumberOfFiles -= 1;
            }
        }

        let localPathToDir: string = rootPathToDir;

        const name: string = path.basename(localPathToDir);
        localPathToDir = options !== undefined && options.normalizePath !== undefined && options.normalizePath ?
            this.normalizePath(localPathToDir) : localPathToDir;

        // setting default values for 'rootPathToDir' path
        let rootPathInfo: IFIleSystemTree | undefined = {
            path: localPathToDir,
            name,
            size: 0,
            mtime: new Date(),
            ctime: new Date(),
            atime: new Date(),
            type: FILE_TYPE.DIRECTORY
        };

        let rootPathStats: fs.Stats;
        try {
            rootPathStats = fs.lstatSync(localPathToDir);
        } catch (e) {
            console.warn(`Unable to find stats for the given path: ${localPathToDir}`);

            return undefined;
        }

        // Skip directory/file, if it matches the exclude regex. Eg - '.*' for all hidden files
        if (options !== undefined && options.exclude !== undefined) {
            const excludes: RegExp[] = this.isRegEx(options.exclude) ? [<RegExp>options.exclude] : <RegExp[]>options.exclude;
            if (excludes.some((exclusion: RegExp): boolean => exclusion.test(localPathToDir))) {
                return undefined;
            }
        }

        if (rootPathStats.isFile()) {
            rootPathInfo = this.getFileDetails(localPathToDir, rootPathInfo, rootPathStats, onEachFile, FILE_TYPE.FILE);
        } else if (rootPathStats.isSymbolicLink()) {
            if (options !== undefined && options.resolveSymbolicLinks !== undefined && options.resolveSymbolicLinks) {
                try {
                    const isDirectory: boolean = fs.lstatSync(fs.realpathSync(localPathToDir))
                        .isDirectory();
                    if (isDirectory) {
                        rootPathInfo = this.getDirectoriesAndFiles(localPathToDir, rootPathInfo, rootPathStats,
                                                                   options, onEachFile, onEachDirectory);
                    } else {
                        rootPathStats = fs.lstatSync(fs.realpathSync(localPathToDir));
                        rootPathInfo = this.getFileDetails(localPathToDir, rootPathInfo, rootPathStats, onEachFile, FILE_TYPE.FILE);
                    }
                } catch (err) {
                    console.log(`lStatSync for '${(localPathToDir)}' failed. Skipping current file.`);

                    return undefined;
                }
            } else {
                rootPathInfo = this.getFileDetails(localPathToDir, rootPathInfo, rootPathStats, onEachFile, FILE_TYPE.SYMB_LINK);
            }
        } else if (rootPathStats.isDirectory()) {
            rootPathInfo = this.getDirectoriesAndFiles(localPathToDir, rootPathInfo, rootPathStats, options, onEachFile, onEachDirectory);
        } else {
            return undefined; // Or set item.size = 0 for devices, FIFO and sockets ?
        }

        return rootPathInfo;
    }

    /**
     * get File Details of given file
     */
    public getFileDetails(localPathToDir: string, rootPathInfo: IFIleSystemTree, rootPathStats: fs.Stats,
                          onEachFile?: Function, fileType: number = FILE_TYPE.FILE): IFIleSystemTree | undefined {
        const fileExtension: string = path.extname(localPathToDir)
            .toLowerCase();

        rootPathInfo.size = rootPathStats.size;  // File size in bytes
        rootPathInfo.extension = fileExtension;
        rootPathInfo.type = fileType;
        rootPathInfo.atime = rootPathStats.atime;
        rootPathInfo.mtime = rootPathStats.mtime;
        rootPathInfo.ctime = rootPathStats.ctime;

        if (onEachFile !== undefined) {
            onEachFile(rootPathInfo, localPathToDir, rootPathStats);
        }

        return rootPathInfo;
    }

    /**
     *  get all directory/file name from the given path
     */
    public getDirectoriesAndFiles(localPathToDir: string, rootPathInfo: IFIleSystemTree, rootPathStats: fs.Stats,
                                  options?: IFileSystemExplorerOptions, onEachFile?: Function, onEachDirectory?: Function):
                                  IFIleSystemTree | undefined {

        try {
            const dirContent: string[] = <string[]>this.safeReadDirSync(localPathToDir);
            if (dirContent === undefined) { return undefined; }

            // recursive call to build new sub-tree in children of rootPath
            rootPathInfo.children = dirContent.map((child: string): IFIleSystemTree => {
                return <IFIleSystemTree>this.createFileSystemTree(path.join(localPathToDir, child), options, onEachFile, onEachDirectory);
            })
            .filter((e: IFIleSystemTree): boolean => {
                return (e !== undefined);
            });

            // using reduce to get size from the child elements
            rootPathInfo.size = rootPathInfo.children.reduce((prev: number, cur: IFIleSystemTree): number => prev + cur.size, 0);
            rootPathInfo.childCount = rootPathInfo.children.length;
            rootPathInfo.type = FILE_TYPE.DIRECTORY;
            rootPathInfo.atime = rootPathStats.atime;
            rootPathInfo.mtime = rootPathStats.mtime;
            rootPathInfo.ctime = rootPathStats.ctime;

            if (onEachDirectory !== undefined) {
                onEachDirectory(rootPathInfo, localPathToDir, rootPathStats);
            }

            return rootPathInfo;
        } catch (err) {
            console.log(`getDirectoriesAndFiles failed. Error: ${err}`);

            return undefined;
        }
    }

    public getDateFormatMMDDYYYY(date: Date): string {
        return `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()} \
${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
    }

    /**
     * @param rootPathToDir path from where FS Tree to be created
     * returns {string[]} array of all sub-directories path from inside rootPathToDir
     */
    public getAllDirectoryPathFromFileSystem(rootPathToDir: string, options?: IFileSystemExplorerOptions, onEachFile?: Function,
                                             onEachDirectory?: Function): string[] {
        const fileSystemListInfo: IFIleSystemTree | undefined =
            this.createFileSystemTree(rootPathToDir, options, onEachFile, onEachDirectory);
        if (fileSystemListInfo === undefined) {
            // return empty array if something went wrong with rootPathToDir
            return [];
        }

        return this.getAllDirectoryPathFromPreparedFSInfo(fileSystemListInfo);
    }

    /**
     * @param fileSystemListInfo prepared file system List Tree
     * returns {string[]} array of all sub-directories
     */
    public getAllDirectoryPathFromPreparedFSInfo(fileSystemListInfo: IFIleSystemTree): string[] {
        const allDirectoriesPath: string[] = [];
        // to log count for number of directories during FT
        let directoryCount: number = 0;

        // Using self-invoking anonymous function for DFS directory tree
        const traverseFileSystemDFS: Function = (fsListInfo: IFIleSystemTree): void => {
            if (fsListInfo.type === FILE_TYPE.DIRECTORY) {
                allDirectoriesPath.push(fsListInfo.path);
                directoryCount += 1;
            }
            fsListInfo.children?.forEach((child: IFIleSystemTree): void => {
                traverseFileSystemDFS(child);
            });
        };
        traverseFileSystemDFS(fileSystemListInfo);

        console.info(`Total Directories Count from Path "${fileSystemListInfo.path}": ${directoryCount}`);

        return allDirectoriesPath;
    }

    /**
     *
     * @param rootPathToDir path from where FS Tree to be created
     * returns {string[]} array of all sub-directories path from inside rootPathToDir
     */
    public getAllFilePathFromFileSystem(rootPathToDir: string, options?: IFileSystemExplorerOptions, onEachFile?: Function,
                                        onEachDirectory?: Function): string[] {
        const fileSystemListInfo: IFIleSystemTree | undefined =
            this.createFileSystemTree(rootPathToDir, options, onEachFile, onEachDirectory);
        if (fileSystemListInfo === undefined) {
            // return empty array if something went wrong with rootPathToDir
            return [];
        }

        return this.getAllFilePathFromPreparedFSInfo(fileSystemListInfo);
    }

    /**
     * @param fileSystemListInfo prepared file system List Tree
     * returns {string[]} array of all sub-files
     */
    public getAllFilePathFromPreparedFSInfo(fileSystemListInfo: IFIleSystemTree): string[] {
        const allFilesPath: string[] = [];
        // to log count for number of directories during FT
        let filesCount: number = 0;

        // Using self-invoking anonymous function for DFS
        const traverseFileSystemDFS: Function = (fsListInfo: IFIleSystemTree): void => {
            if (fsListInfo.type === FILE_TYPE.FILE) {
                allFilesPath.push(fsListInfo.path);
                filesCount += 1;
            }
            fsListInfo.children?.forEach((child: IFIleSystemTree): void => {
                traverseFileSystemDFS(child);
            });
        };
        traverseFileSystemDFS(fileSystemListInfo);

        // console.info(`Total Files Count from path "${fileSystemListInfo.path}": ${filesCount}`);

        return allFilesPath;
    }

    /**
     * @param rootPathToDir path from where FS Tree to be created
     * returns {IDirFileInfo[]} array of all sub-directories details from inside rootPathToDir
     */
    public getAllFSElementsDetailsFromFileSystem(rootPathToDir: string, options?: IFileSystemExplorerOptions, onEachFile?: Function,
                                                 onEachDirectory?: Function): IFSInfo[] {
        const fileSystemListInfo: IFIleSystemTree | undefined =
            this.createFileSystemTree(rootPathToDir, options, onEachFile, onEachDirectory);
        if (fileSystemListInfo === undefined) {
            // return empty array if something went wrong with rootPathToDir
            return [];
        }

        return (this.getAllFSElementsDetailsFromPreparedFSInfo(fileSystemListInfo));
    }

    /**
     * @param fileSystemListInfo prepared file system List Tree
     * returns {IDirFileInfo[]} array of all sub-directories details
     */
    public getAllFSElementsDetailsFromPreparedFSInfo(fileSystemListInfo: IFIleSystemTree): IFSInfo[] {
        const allFSElementsDetails: IFSInfo[] = [];
        // to log count for number of directories during FT

        let eachFSElement: IFSInfo = {
            type: FILE_TYPE.FILE,
            size: 0,
            name: '',
            mtime: '',
            path: ''
        };
        // Using self-invoking anonymous function for DFS directory tree
        const traverseFileSystemDFS: Function = (fsListInfo: IFIleSystemTree): void => {
            eachFSElement = {
                type: fsListInfo.type,
                size: fsListInfo.size,
                name: fsListInfo.name,
                mtime: this.getDateFormatMMDDYYYY(fsListInfo.mtime),
                path: fsListInfo.path,
                extension: fsListInfo.extension,
                childCount: fsListInfo.childCount
            };
            allFSElementsDetails.push(eachFSElement);
            fsListInfo.children?.forEach((child: IFIleSystemTree): void => {
                traverseFileSystemDFS(child);
            });
        };
        traverseFileSystemDFS(fileSystemListInfo);

        // console.info(`Total FS Elements Count from Path "${fileSystemListInfo.path}": ${allFSElementsDetails.length}`);

        return allFSElementsDetails;
    }

    /**
     * @param pathToDir path from the list of dir/files to be retrieved
     * returns IDirFileInfo[] which have information (mtime, size, name)
     */
    public getDirectoryContentList(pathToDir: string, options?: IFileSystemExplorerOptions, onEachDirContent?: Function): IFSInfo[] {
        let dirContentList: IFSInfo[] = [];
        let localPathToDir: string = pathToDir;

        try {
            const dirContentNames: string[] | undefined = this.safeReadDirSync(pathToDir);
            if (dirContentNames === undefined) { return []; }

            localPathToDir = options !== undefined && options.normalizePath !== undefined && options.normalizePath ?
                this.normalizePath(localPathToDir) : localPathToDir;

            // Skip directory/file, if it matches the exclude regex. Eg - '.*' for all hidden files
            if (options !== undefined && options.exclude !== undefined) {
                const excludes: RegExp[] = this.isRegEx(options.exclude) ? [<RegExp>options.exclude] : <RegExp[]>options.exclude;
                if (excludes.some((exclusion: RegExp): boolean => exclusion.test(localPathToDir))) {
                    return [];
                }
            }

            let dirFileInfo: IFSInfo;

            dirContentList = <IFSInfo[]>dirContentNames.map((eachDirFileName: string): IFSInfo | undefined => {
                dirFileInfo = {
                    name: eachDirFileName,
                    size: 0,
                    mtime: this.getDateFormatMMDDYYYY(new Date()),
                    type: FILE_TYPE.FILE
                };

                // getting info for each content
                let eachDirFileStats: fs.Stats;
                try {
                    eachDirFileStats = fs.lstatSync(path.join(localPathToDir, eachDirFileName));
                } catch (e) {
                    console.warn(`Unable to find stats for the given path: ${localPathToDir}`);

                    return undefined;
                }
                // assigning each property based in lstat output
                if (!isNaN(eachDirFileStats.mtime.getFullYear())) {
                    dirFileInfo.mtime = this.getDateFormatMMDDYYYY(eachDirFileStats.mtime);
                } else {
                    const pathToUse: string = path.join(localPathToDir.replace(/[\\&"')( ]/g, '\\$&'),
                                                        eachDirFileName.replace(/[\\&"')( ]/g, '\\$&'));
                    dirFileInfo.mtime = this.checkForPathAndFindTime(pathToUse);
                }
                dirFileInfo.size = eachDirFileStats.size;  // File size in bytes

                if (eachDirFileStats.isFile() || eachDirFileStats.isSymbolicLink()) {
                    const fileExtension: string = path.extname(eachDirFileName)
                        .toLowerCase();

                    dirFileInfo.extension = fileExtension;
                    dirFileInfo.type = eachDirFileStats.isFile() ? FILE_TYPE.FILE : FILE_TYPE.SYMB_LINK;

                } else if (eachDirFileStats.isDirectory()) {
                    dirFileInfo.type = FILE_TYPE.DIRECTORY;
                } else {
                    // if file type is except file, directory or symbolic link, then skipping it
                    return undefined;
                }

                if (onEachDirContent !== undefined) {
                    onEachDirContent(path.join(localPathToDir, eachDirFileName), eachDirFileStats);
                }

                return dirFileInfo;
            })
                .filter((e: IFSInfo | undefined): boolean => {
                    return (e !== undefined);
                });

            return dirContentList;
        } catch (err) {
            console.log(`getDirectoryContentList failed. Error: ${err}`);

            return [];
        }
    }

    /**
     * @param pathToDir path from the list of dir/files to be retrieved
     * returns IDirFileInfo[] which have information (mtime, size, name)
     */
    public getDirectoryContentListForJSON(pathToDir: string, options?: IFileSystemExplorerOptions,
                                          onEachDirContent?: Function): IFSNewInfo[] {
        let dirContentList: IFSNewInfo[] = [];
        let localPathToDir: string = pathToDir;

        try {
            const dirContentNames: string[] | undefined = this.safeReadDirSync(pathToDir);
            if (dirContentNames === undefined) { return []; }

            localPathToDir = options !== undefined && options.normalizePath !== undefined && options.normalizePath ?
                this.normalizePath(localPathToDir) : localPathToDir;

            // Skip directory/file, if it matches the exclude regex. Eg - '.*' for all hidden files
            if (options !== undefined && options.exclude !== undefined) {
                const excludes: RegExp[] = this.isRegEx(options.exclude) ? [<RegExp>options.exclude] : <RegExp[]>options.exclude;
                if (excludes.some((exclusion: RegExp): boolean => exclusion.test(localPathToDir))) {
                    return [];
                }
            }

            let dirFileInfo: IFSNewInfo;

            dirContentList = <IFSNewInfo[]>dirContentNames.map((eachDirFileName: string): IFSNewInfo | undefined => {
                dirFileInfo = {
                    m_name: eachDirFileName,
                    lsize: '0',
                    m_lmd: this.getDateFormatMMDDYYYY(new Date()),
                    nType: FILE_TYPE.FILE,
                    m_FullPath: path.join(pathToDir, eachDirFileName)
                };

                // getting info for each content
                let eachDirFileStats: fs.Stats;
                try {
                    eachDirFileStats = fs.lstatSync(path.join(localPathToDir, eachDirFileName));
                } catch (e) {
                    console.warn(`Unable to find stats for the given path: ${localPathToDir}`);

                    return undefined;
                }
                // assigning each property based in lstat output
                if (!isNaN(eachDirFileStats.mtime.getFullYear())) {
                    dirFileInfo.m_lmd = this.getDateFormatMMDDYYYY(eachDirFileStats.mtime);
                } else {
                    const pathToUse: string = path.join(localPathToDir.replace(/[\\&"')( ]/g, '\\$&'),
                                                        eachDirFileName.replace(/[\\&"')( ]/g, '\\$&'));
                    dirFileInfo.m_lmd = this.checkForPathAndFindTime(pathToUse);
                }
                dirFileInfo.lsize = eachDirFileStats.size.toString();  // File size in bytes

                if (eachDirFileStats.isFile() || eachDirFileStats.isSymbolicLink()) {
                    const fileExtension: string = path.extname(eachDirFileName)
                        .toLowerCase();

                    dirFileInfo.extension = fileExtension;
                    dirFileInfo.nType = eachDirFileStats.isFile() ? FILE_TYPE.FILE : FILE_TYPE.SYMB_LINK;

                } else if (eachDirFileStats.isDirectory()) {
                    dirFileInfo.nType = FILE_TYPE.DIRECTORY;
                } else {
                    // if file type is except file, directory or symbolic link, then skipping it
                    return undefined;
                }

                if (onEachDirContent !== undefined) {
                    onEachDirContent(path.join(localPathToDir, eachDirFileName), eachDirFileStats);
                }

                return dirFileInfo;
            })
                .filter((e: IFSNewInfo | undefined): boolean => {
                    return (e !== undefined);
                });

            return dirContentList;
        } catch (err) {
            console.log(`getDirectoryContentList failed. Error: ${err}`);

            return [];
        }
    }

    public checkForPathAndFindTime(pathToUse?: string): string {
        try {
            let dateTimeArray: string[] = [];
            let dateFormat: string[] = [];
            let timeFormat: string[] = [];
            const dateBefore1970: string = (execSync(`stat -c %y ${pathToUse}`))
            .toString()
            .trim();
            dateTimeArray = dateBefore1970.split(' ');
            dateFormat = dateTimeArray[0].split('-');
            timeFormat = dateTimeArray[1].split('.');

            return `${dateFormat[1]}/${dateFormat[2]}/${dateFormat[0]} ${timeFormat[0]}`;
        } catch (err) {
            console.error(`getDirectoryDate() => ${err}`);

            return 'Invalid Date';
        }
    }

    public createDirectory(destinationPath: string, isNewDirectoryNameIncluded: boolean = false,
                           isRecursive: boolean = false): IFSOperationResponse {
        const mkdirResponse: IFSOperationResponse = {
            errno: FS_OK,
            completePath: '',
            fileName: ''
        };

        try {
            const newDirectoryName: string = isNewDirectoryNameIncluded ? path.basename(destinationPath) : 'New Folder';
            const destinationPathWithoutDirname: string = isNewDirectoryNameIncluded ?
                destinationPath.slice(0, destinationPath.lastIndexOf(newDirectoryName)) : destinationPath;
            let newFile: string = newDirectoryName;

            if (isNewDirectoryNameIncluded) {
                console.log(`Path: ${destinationPathWithoutDirname}, Dirname: ${newDirectoryName}`);
                fs.mkdirSync(path.join(destinationPathWithoutDirname, newDirectoryName), {recursive: isRecursive});
            } else {
                let i: number;

                if (fs.existsSync(path.join(destinationPathWithoutDirname, newFile))) {
                    console.info(`Directory name 'New Folder' already exists in ${newFile}. Appending number.`);

                    // Append number at the end, and then re-check if file exists
                    i = 1;
                    newFile = `${newDirectoryName} ${(i)}`;
                    while (fs.existsSync(path.join(destinationPathWithoutDirname, newFile))) {
                        i = i + 1;
                        newFile = `${newDirectoryName} ${(i)}`;
                    }
                }

                fs.mkdirSync(path.join(destinationPathWithoutDirname, newFile));
            }
            mkdirResponse.errno = FS_OK;
            mkdirResponse.completePath = path.join(destinationPathWithoutDirname, newFile);
            mkdirResponse.fileName = newFile;

            return mkdirResponse;
        } catch (err) {
            console.error(`Create Directory inside '${destinationPath}' failed. Errno: ${(<NodeJS.ErrnoException>err).errno} ${(<NodeJS.ErrnoException>err).message}`);

            mkdirResponse.errno = (<NodeJS.ErrnoException>err).errno !== undefined ? <number>(<NodeJS.ErrnoException>err).errno : FS_ERR;
            mkdirResponse.completePath = (<NodeJS.ErrnoException>err).message;

            return mkdirResponse;
        }
    }

    public deleteDirectory(destinationPath: string, isDirectory: boolean = false): IFSOperationResponse {
        const rmdirResponse: IFSOperationResponse = {
            errno: FS_OK,
            completePath: '',
            fileName: ''
        };

        try {
            console.debug(`Checking if ${destinationPath} exists or not`);
            if (fs.existsSync(destinationPath)) {
                if (isDirectory) {
                    console.info(`Deleting Directory tree '${destinationPath}'`);
                    fs.rmdirSync(destinationPath, { recursive: true });
                } else {
                    console.info(`Deleting File '${destinationPath}'`);
                    fs.unlinkSync(destinationPath);
                }
            }

            return rmdirResponse;
        } catch (err) {
            console.error(`Deleting Directory '${destinationPath}' failed. Errno: ${(<NodeJS.ErrnoException>err).errno} ${(<NodeJS.ErrnoException>err).message}`);

            rmdirResponse.errno = (<NodeJS.ErrnoException>err).errno !== undefined ? <number>(<NodeJS.ErrnoException>err).errno : FS_ERR;
            rmdirResponse.completePath = (<NodeJS.ErrnoException>err).message;

            return rmdirResponse;
        }
    }

    // throws an error if source and destination are not free/exist. Eg - not a directory, no such file or dir, invalid argument
    public renameFile(oldPath: string, newPath: string): IFSOperationResponse {
        const renameFileResp: IFSOperationResponse = {
            errno: FS_OK,
            completePath: '',
            fileName: ''
        };

        try {
            console.debug(`check if destination path is available or not`);
            if (fs.existsSync(newPath)) {
                renameFileResp.errno = 13;
                renameFileResp.completePath = 'Destination Path already exists';

                return renameFileResp;
            }

            if (fs.existsSync(oldPath)) {
                console.info(`Renaming "${oldPath}" to "${newPath}"`);
                fs.renameSync(oldPath, newPath);
            }

            return renameFileResp;
        } catch (err) {
            console.error(`Rename File Failed: Errno: ${(<NodeJS.ErrnoException>err).errno}. ${(<NodeJS.ErrnoException>err).message}`);

            renameFileResp.errno = (<NodeJS.ErrnoException>err).errno !== undefined ? <number>(<NodeJS.ErrnoException>err).errno : FS_ERR;
            renameFileResp.completePath = (<NodeJS.ErrnoException>err).message;

            return renameFileResp;
        }
    }

    public checkDirectoryReadWritePersmission(pathToDir: string, mode: number = fs.constants.W_OK): number {
        try {
            fs.accessSync(pathToDir, mode);
            console.info(`${pathToDir} is accessible for mode: ${mode}`);

            return FS_OK;
        } catch (err) {
            if ((<NodeJS.ErrnoException>err).code === 'EACCES' || (<NodeJS.ErrnoException>err).code === 'EPERM') {
                console.warn(`User doesn't have access to "${pathToDir}". Ignoring Directory`);
            } else {
                console.warn(`Unable to Read from given path "${pathToDir}". Errno: ${(<NodeJS.ErrnoException>err).errno}. Details: ${(<NodeJS.ErrnoException>err).message}`);
            }

            return <number>(<NodeJS.ErrnoException>err).errno;
        }
    }

    public checkFSElementExists(pathFSElement: string): boolean {
        try {
            if (fs.existsSync(pathFSElement)) {
                return true;
            } else {
                return false;
            }
        } catch (err) {
            if ((<NodeJS.ErrnoException>err).code === 'EACCES' || (<NodeJS.ErrnoException>err).code === 'EPERM') {
                console.warn(`User doesn't have access to "${pathFSElement}". Ignoring Directory`);
            } else {
                console.warn(`Unable to Read from given path "${pathFSElement}". Errno: ${(<NodeJS.ErrnoException>err).errno}. Details: ${(<NodeJS.ErrnoException>err).message}`);
            }

            return false;
        }
    }

    public getFSElementDetails(fsElementPath: string): IFSInfo {
        const fsElementDetails: IFSInfo = {
            name: path.basename(fsElementPath),
            path: fsElementPath,
            type: FILE_TYPE.FILE,
            mtime: this.getDateFormatMMDDYYYY(new Date()),
            size: 0
        };
        try {
            const fsElementStats: fs.Stats = fs.lstatSync(fsElementPath);
            fsElementDetails.type = fsElementStats.isDirectory() ? FILE_TYPE.DIRECTORY :
                (fsElementStats.isFile() ? FILE_TYPE.FILE : FILE_TYPE.SYMB_LINK);
            if (!isNaN(fsElementStats.mtime.getFullYear())) {
                fsElementDetails.mtime = this.getDateFormatMMDDYYYY(fsElementStats.mtime);
            } else {
                fsElementDetails.mtime = this.checkForPathAndFindTime(fsElementDetails.path?.replace(/[\\&"')( ]/g, '\\$&'));
            }
            fsElementDetails.size = fsElementStats.size;
            fsElementDetails.extension = fsElementDetails.type === FILE_TYPE.FILE ? path.extname(fsElementPath)
                .toLowerCase() : undefined;

            return fsElementDetails;
        } catch (e) {
            console.warn(`Unable to find stats for the given path: ${fsElementPath}`);

            return fsElementDetails;
        }
    }

    /**
     * Return free space available in bytes for mount which contain the file. If command fails, return 0
     * @param pathToFSElement path for new file. Check if free space available for the mount in which this file is present
     */
    public checkMountFreeSpace(pathToFSElement: string): string {
        try {
            const mountNameForFSElement: string =
                (execSync(`df "${pathToFSElement}" | tail -1 | awk '{ print $6 }'`)).toString()
                .trim();
            // const freeSpaceInBytes: string = (execSync(`df -h ${mountNameForFSElement} --block-size=1024 | tail -1 | awk '{print $4}'`))
            //     .toString()
            //     .trim();
            // console.debug(`${pathToFSElement} belongs to '${mountNameForFSElement}'. Free space: ${freeSpaceInBytes} bytes`);

            return (execSync(`df -h ${mountNameForFSElement} --block-size=1024 | tail -1 | awk '{print $4}'`))
                .toString()
                .trim();
        } catch (err) {
            console.info(`Failed to execute command to get free space from mount. ${(<Error>err).stack}`);

            return '0';
        }
    }

    /**
     * Normalizes windows style paths by replacing double backslahes with single forward slahes (unix style).
     */
    public normalizePath(absolutePath: string): string {
        return absolutePath.replace(/\\/g, '/');
    }

    /**
     * @param path aboslute path to be safely read
     * return {string[]} return array of string containing name of all sub-directories and files name
     */
    private safeReadDirSync(absolutePath: string): string[] | undefined {
        let dirContents: string[] = [];
        try {
            dirContents = fs.readdirSync(absolutePath);
        } catch (err) {
            if ((<NodeJS.ErrnoException>err).code === 'EACCES' || (<NodeJS.ErrnoException>err).code === 'EPERM') {
                console.warn(`User doesn't have access to "${absolutePath}". Ignoring Directory`);
            } else {
                console.warn(`Unable to Read from given path "${absolutePath}". Errno: ${(<NodeJS.ErrnoException>err).errno}. Details: ${(<NodeJS.ErrnoException>err).message}`);
            }

            return undefined;
        }

        return dirContents;

    }

    /**
     * Tests if the supplied parameter is regex or not
     */
    private isRegEx(regExp: Object | RegExp | string): boolean {
        return typeof regExp === 'object' && regExp.constructor === RegExp;
    }
}
